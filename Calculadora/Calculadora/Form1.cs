﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnMult_Click(object sender, EventArgs e)
        {
            
      
        }

        private void btnDiv_Click(object sender, EventArgs e)
        {

        }

        private void btnCero_Click(object sender, EventArgs e)
        {
            textEntrada.Text += "0";
        }

        private void btnUno_Click(object sender, EventArgs e)
        {
            textEntrada.Text += "1";
        }

        private void btnDos_Click(object sender, EventArgs e)
        {
            textEntrada.Text += "2";
        }

        private void btnTres_Click(object sender, EventArgs e)
        {
            textEntrada.Text += "3";
        }

        private void btnCuatro_Click(object sender, EventArgs e)
        {
            textEntrada.Text += "4";
        }

        private void btnCinco_Click(object sender, EventArgs e)
        {
            textEntrada.Text += "5";
        }

        private void btnSeis_Click(object sender, EventArgs e)
        {
            textEntrada.Text += "6";
        }

        private void btnSiete_Click(object sender, EventArgs e)
        {
            textEntrada.Text += "7";
        }

        private void btnOcho_Click(object sender, EventArgs e)
        {
            textEntrada.Text += "8";
        }

        private void btnNueve_Click(object sender, EventArgs e)
        {
            textEntrada.Text += "9";
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            textEntrada.Text = textEntrada.Text.Substring(0, textEntrada.Text.Count() - 1);
        }
    }
}
