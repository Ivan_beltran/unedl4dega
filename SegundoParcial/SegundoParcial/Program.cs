﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SegundoParcialTasks
{
    class Program
    {
        private static Semaphore pooldechalanes;
        private static int n = 0;
        private static int m = 0;
        private static int o = 0;
       

        public static void casa(int []o)
        {
            int cua = o[1];
            int bañ = o[2];
            
            //Inicio con la construccion
            Console.WriteLine("Iniciando la casa {0}", o[0]);
            pooldechalanes.WaitOne();
            var cuartos = new Task[cua];
            var baños = new Task[bañ];
            var rdn = new Random();


            for (int i = 0; i <= cuartos.Length - 1; i++)
            {
                cuartos[i] = Task.Run(() => Thread.Sleep(rdn.Next(500, 3000)));
                
            }
            for (int i = 0; i <= baños.Length - 1; i++)
            {
                
                baños[i] = Task.Run(() => Thread.Sleep(rdn.Next(500, 3000)));
            }
            try
            {
                Task.WaitAll(cuartos);
                Task.WaitAll(baños);
                Console.WriteLine("------------------");
            }
            catch (AggregateException ae)
            {
                Console.WriteLine("Falta material");
                
            }
            for(int x = 0; x < cuartos.Length; x++) { 
                Console.WriteLine(" cuarto: {0}: {1}",x+1, cuartos[x].Status);
                 }

            for (int x = 0; x < baños.Length; x++)
            {
                Console.WriteLine(" baño: {0}: {1}", x + 1, baños[x].Status);
            }
            

            Console.WriteLine(" CASA {0} TERMINADA, turno {1}", o[0],
                pooldechalanes.Release());
        }

        public static void Main(string[] args)
        {
            
            var bañ = 0;
            var cua = 0;

            //Pedimos datos al usuario
            Console.WriteLine("¿Cuántas casas?");
            n = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿Cuántos chalanes pueden trabajar?");
            m = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿Cuántos segundos hay para contruir?");
            o = Convert.ToInt32(Console.ReadLine()) * 1000;

            Console.WriteLine("¿Cuantos cuartos?");
            cua = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿Cuantos baños?");
            bañ = Convert.ToInt32(Console.ReadLine());

            pooldechalanes = new Semaphore(0, m);

            Console.WriteLine("Inicia la construcción");

            //Arranco los hilos con while por que no podia poner el contador del for en el Start debido a que use Lambda
            int j = 1;
            while (j <= n)
            {   
                //Este arreglo contiene todos los parametros que necesito para trabajar en el metodo
                int[] parametros = { j, cua, bañ };
                Thread t = new Thread(() => casa(parametros));
                t.Start();
                j++;
            }

            Thread.Sleep(500);
            Console.WriteLine("Se libera un turno");
            pooldechalanes.Release(m);

            Thread.Sleep(o);

            Console.WriteLine("Construcción terminada");
            Console.ReadKey();
        }
    }
}