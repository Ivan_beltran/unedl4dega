﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalculadoraIngresos
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Aguinaldo()
        {
            double sueldo = Double.Parse(txtSueldo.Text);
            double nomina = Double.Parse(txtNomina.Text);
            double resultado;

            resultado = (sueldo / nomina) * 15;

            lblAguinaldo.Text = "$" + Convert.ToString(Math.Round(resultado, 2));
        }

        private void Vacaciones()
        {
            double sueldo = Double.Parse(txtSueldo.Text);
            double nomina = Double.Parse(txtNomina.Text);
            double vacaciones = Double.Parse(txtVacaciones.Text);
            double resultado;

            resultado = ((sueldo / nomina) * vacaciones) * 0.25;

            lblVacaciones.Text = "$" + Convert.ToString(Math.Round(Math.Round(resultado, 2), 2));
        }
        private void infonavit()
        {
            double sueldo = Double.Parse(txtSueldo.Text);
            double nomina = Double.Parse(txtNomina.Text);
            double resultado;

            resultado = sueldo * 0.05;

            lblInfonavit.Text = "$" + Convert.ToString(Math.Round(resultado, 2));
        }
        private void IMSS()
        {
            double sueldo = Double.Parse(txtSueldo.Text);
            double nomina = Double.Parse(txtNomina.Text);
            double resultado;

            double auxA, auxB, auxC;

            auxA = sueldo * 0.0025;
            auxB = sueldo * 0.00375;
            auxC = sueldo * 0.00625;

            resultado = auxA + auxB + auxC;

            lblIMSS.Text = "$" + Convert.ToString(Math.Round(resultado, 2));
        }
        private void RCV()
        {
            double sueldo = Double.Parse(txtSueldo.Text);
            double nomina = Double.Parse(txtNomina.Text);
            double resultado;

            resultado = (sueldo * 0.01125) * 2;

            lblRCV.Text = "$" + Convert.ToString(Math.Round(resultado, 2));
        }
        private void despensa()
        {
            double sueldo = Double.Parse(txtSueldo.Text);
            double nomina = Double.Parse(txtNomina.Text);
            double resultado;

            resultado = sueldo * 0.01;

            lblDespensa.Text = "$" + Convert.ToString(Math.Round(resultado, 2));
        }


        private void btnCalcular_Click(object sender, EventArgs e)
        {
            Aguinaldo();
            Vacaciones();
            infonavit();
            IMSS();
            RCV();
            despensa();
        }

    }
}
