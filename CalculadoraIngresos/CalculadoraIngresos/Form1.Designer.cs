﻿namespace CalculadoraIngresos
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtSueldo = new System.Windows.Forms.TextBox();
            this.txtNomina = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblAguinaldo = new System.Windows.Forms.Label();
            this.lblVacaciones = new System.Windows.Forms.Label();
            this.lblRCV = new System.Windows.Forms.Label();
            this.lblIMSS = new System.Windows.Forms.Label();
            this.lblInfonavit = new System.Windows.Forms.Label();
            this.lblDespensa = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtVacaciones = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtSueldo
            // 
            this.txtSueldo.Location = new System.Drawing.Point(73, 32);
            this.txtSueldo.Name = "txtSueldo";
            this.txtSueldo.Size = new System.Drawing.Size(100, 20);
            this.txtSueldo.TabIndex = 0;
            // 
            // txtNomina
            // 
            this.txtNomina.Location = new System.Drawing.Point(73, 66);
            this.txtNomina.Name = "txtNomina";
            this.txtNomina.Size = new System.Drawing.Size(100, 20);
            this.txtNomina.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Sueldo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nomina";
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(214, 35);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(133, 57);
            this.btnCalcular.TabIndex = 4;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Aguinaldo:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 169);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Vacaciones:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(196, 169);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Infonavit:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(196, 138);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "IMSS:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 208);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "RCV:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(196, 208);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Despensa:";
            // 
            // lblAguinaldo
            // 
            this.lblAguinaldo.AutoSize = true;
            this.lblAguinaldo.Location = new System.Drawing.Point(82, 138);
            this.lblAguinaldo.Name = "lblAguinaldo";
            this.lblAguinaldo.Size = new System.Drawing.Size(0, 13);
            this.lblAguinaldo.TabIndex = 11;
            // 
            // lblVacaciones
            // 
            this.lblVacaciones.AutoSize = true;
            this.lblVacaciones.Location = new System.Drawing.Point(85, 169);
            this.lblVacaciones.Name = "lblVacaciones";
            this.lblVacaciones.Size = new System.Drawing.Size(0, 13);
            this.lblVacaciones.TabIndex = 12;
            // 
            // lblRCV
            // 
            this.lblRCV.AutoSize = true;
            this.lblRCV.Location = new System.Drawing.Point(57, 207);
            this.lblRCV.Name = "lblRCV";
            this.lblRCV.Size = new System.Drawing.Size(0, 13);
            this.lblRCV.TabIndex = 13;
            // 
            // lblIMSS
            // 
            this.lblIMSS.AutoSize = true;
            this.lblIMSS.Location = new System.Drawing.Point(239, 137);
            this.lblIMSS.Name = "lblIMSS";
            this.lblIMSS.Size = new System.Drawing.Size(0, 13);
            this.lblIMSS.TabIndex = 14;
            // 
            // lblInfonavit
            // 
            this.lblInfonavit.AutoSize = true;
            this.lblInfonavit.Location = new System.Drawing.Point(254, 169);
            this.lblInfonavit.Name = "lblInfonavit";
            this.lblInfonavit.Size = new System.Drawing.Size(0, 13);
            this.lblInfonavit.TabIndex = 15;
            // 
            // lblDespensa
            // 
            this.lblDespensa.AutoSize = true;
            this.lblDespensa.Location = new System.Drawing.Point(261, 207);
            this.lblDespensa.Name = "lblDespensa";
            this.lblDespensa.Size = new System.Drawing.Size(0, 13);
            this.lblDespensa.TabIndex = 16;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(18, 98);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Vacaciones:";
            // 
            // txtVacaciones
            // 
            this.txtVacaciones.Location = new System.Drawing.Point(85, 95);
            this.txtVacaciones.Name = "txtVacaciones";
            this.txtVacaciones.Size = new System.Drawing.Size(40, 20);
            this.txtVacaciones.TabIndex = 18;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(352, 230);
            this.Controls.Add(this.txtVacaciones);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lblDespensa);
            this.Controls.Add(this.lblInfonavit);
            this.Controls.Add(this.lblIMSS);
            this.Controls.Add(this.lblRCV);
            this.Controls.Add(this.lblVacaciones);
            this.Controls.Add(this.lblAguinaldo);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtNomina);
            this.Controls.Add(this.txtSueldo);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtSueldo;
        private System.Windows.Forms.TextBox txtNomina;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblAguinaldo;
        private System.Windows.Forms.Label lblVacaciones;
        private System.Windows.Forms.Label lblRCV;
        private System.Windows.Forms.Label lblIMSS;
        private System.Windows.Forms.Label lblInfonavit;
        private System.Windows.Forms.Label lblDespensa;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtVacaciones;
    }
}

