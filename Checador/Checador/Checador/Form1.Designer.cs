﻿namespace Checador
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblEmpleado = new System.Windows.Forms.Label();
            this.cbxEmpleado = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.gbxOperacion = new System.Windows.Forms.GroupBox();
            this.rdbtEntrada = new System.Windows.Forms.RadioButton();
            this.rdbtSalida = new System.Windows.Forms.RadioButton();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.btnRevisar = new System.Windows.Forms.Button();
            this.gbxOperacion.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblEmpleado
            // 
            this.lblEmpleado.AutoSize = true;
            this.lblEmpleado.Location = new System.Drawing.Point(9, 39);
            this.lblEmpleado.Name = "lblEmpleado";
            this.lblEmpleado.Size = new System.Drawing.Size(57, 13);
            this.lblEmpleado.TabIndex = 0;
            this.lblEmpleado.Text = "Empleado:";
            // 
            // cbxEmpleado
            // 
            this.cbxEmpleado.FormattingEnabled = true;
            this.cbxEmpleado.Items.AddRange(new object[] {
            "Ivan Beltra",
            "Carlos Aguilar",
            "Toño Diego",
            "Omar Herrera"});
            this.cbxEmpleado.Location = new System.Drawing.Point(83, 36);
            this.cbxEmpleado.Name = "cbxEmpleado";
            this.cbxEmpleado.Size = new System.Drawing.Size(181, 21);
            this.cbxEmpleado.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Fecha/Hora:";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(83, 89);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 3;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // gbxOperacion
            // 
            this.gbxOperacion.Controls.Add(this.rdbtSalida);
            this.gbxOperacion.Controls.Add(this.rdbtEntrada);
            this.gbxOperacion.Location = new System.Drawing.Point(25, 145);
            this.gbxOperacion.Name = "gbxOperacion";
            this.gbxOperacion.Size = new System.Drawing.Size(257, 51);
            this.gbxOperacion.TabIndex = 4;
            this.gbxOperacion.TabStop = false;
            this.gbxOperacion.Text = "Operacion";
            // 
            // rdbtEntrada
            // 
            this.rdbtEntrada.AutoSize = true;
            this.rdbtEntrada.Location = new System.Drawing.Point(16, 19);
            this.rdbtEntrada.Name = "rdbtEntrada";
            this.rdbtEntrada.Size = new System.Drawing.Size(62, 17);
            this.rdbtEntrada.TabIndex = 0;
            this.rdbtEntrada.TabStop = true;
            this.rdbtEntrada.Text = "Entrada";
            this.rdbtEntrada.UseVisualStyleBackColor = true;
            this.rdbtEntrada.CheckedChanged += new System.EventHandler(this.rdbtEntrada_CheckedChanged);
            // 
            // rdbtSalida
            // 
            this.rdbtSalida.AutoSize = true;
            this.rdbtSalida.Location = new System.Drawing.Point(140, 20);
            this.rdbtSalida.Name = "rdbtSalida";
            this.rdbtSalida.Size = new System.Drawing.Size(54, 17);
            this.rdbtSalida.TabIndex = 1;
            this.rdbtSalida.TabStop = true;
            this.rdbtSalida.Text = "Salida";
            this.rdbtSalida.UseVisualStyleBackColor = true;
            this.rdbtSalida.CheckedChanged += new System.EventHandler(this.rdbtSalida_CheckedChanged);
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.Enabled = false;
            this.btnRegistrar.Location = new System.Drawing.Point(41, 220);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(75, 23);
            this.btnRegistrar.TabIndex = 5;
            this.btnRegistrar.Text = "Registrar";
            this.btnRegistrar.UseVisualStyleBackColor = true;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click);
            // 
            // btnRevisar
            // 
            this.btnRevisar.Enabled = false;
            this.btnRevisar.Location = new System.Drawing.Point(165, 220);
            this.btnRevisar.Name = "btnRevisar";
            this.btnRevisar.Size = new System.Drawing.Size(75, 23);
            this.btnRevisar.TabIndex = 6;
            this.btnRevisar.Text = "Revisar";
            this.btnRevisar.UseVisualStyleBackColor = true;
            this.btnRevisar.Click += new System.EventHandler(this.btnRevisar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(311, 302);
            this.Controls.Add(this.btnRevisar);
            this.Controls.Add(this.btnRegistrar);
            this.Controls.Add(this.gbxOperacion);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbxEmpleado);
            this.Controls.Add(this.lblEmpleado);
            this.Name = "Form1";
            this.Text = "Form1";
            this.gbxOperacion.ResumeLayout(false);
            this.gbxOperacion.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblEmpleado;
        private System.Windows.Forms.ComboBox cbxEmpleado;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.GroupBox gbxOperacion;
        private System.Windows.Forms.RadioButton rdbtSalida;
        private System.Windows.Forms.RadioButton rdbtEntrada;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.Button btnRevisar;
    }
}

