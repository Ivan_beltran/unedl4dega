﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Checador
{
    public partial class Form1 : Form
    {

        
        DateTime Entrada = new DateTime();
        DateTime tiempo = new DateTime();
        DateTime Salida = new DateTime();
        string empleado;

        
        int operacion = 0;

        public string archivo = @"C:\Users\Toshiba\desktop\EmpleadosRegistrados.txt";


        public Form1()
        {
            InitializeComponent();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            empleado = cbxEmpleado.Text;

            if (!File.Exists(archivo))
            {
                using (StreamWriter sw = File.CreateText(archivo))
                {
                    sw.WriteLine(empleado);
                    sw.WriteLine(Entrada);
                    sw.WriteLine(Salida);
                }
            }
            else
            {
                string append = "";
                if (operacion == 1)
                {
                    append = empleado + "\nEntrada\n" + Entrada + Environment.NewLine;
                }
                else if (operacion == 2)
                {
                    append = empleado + "\nSalida\n" + Salida + Environment.NewLine;
                }

                File.AppendAllText(archivo, append);
                //Entrada = tiempo;

            }

        }

        private void btnRevisar_Click(object sender, EventArgs e)
        {
            string readText = File.ReadAllText(archivo);
            MessageBox.Show(readText);
        }

        private void rdbtEntrada_CheckedChanged(object sender, EventArgs e)
        {
            btnRegistrar.Enabled = true;
            btnRevisar.Enabled = true;
            operacion = 1;
        }

        private void rdbtSalida_CheckedChanged(object sender, EventArgs e)
        {
            Salida = dateTimePicker1.Value;
            btnRegistrar.Enabled = true;
            btnRevisar.Enabled = true;
            operacion = 2;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            Entrada = dateTimePicker1.Value;
        }
    }
}
